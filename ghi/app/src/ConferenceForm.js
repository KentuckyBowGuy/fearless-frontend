import React, {useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPres, setMaxPres] = useState('');
    const [maxAtt, setMaxAtt] = useState('');
    const [location, setLocation] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPres;
        data.max_attendees = maxAtt;
        data.location = event.target.location.value;

        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPres('');
            setMaxAtt('');
            setLocation('');
        }
    }


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxPresChange = (event) => {
        const value = event.target.value;
        setMaxPres(value);
    }

    const handleMaxAttChange = (event) => {
        const value = event.target.value;
        setMaxAtt(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }




    const fetchData = async () => {
        const locUrl = 'http://localhost:8000/api/locations/';

        const response = await fetch(locUrl);

        if (response.ok) {
            const locData = await response.json();
            setLocations(locData.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit = {handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange= {handleNameChange} placeholder="Name" value={name} required type="text" name ="name" id="name" className="form-control"></input>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {handleStartsChange} placeholder="Starts" value={starts} required type="date" name ="starts" id="starts" className="form-control"></input>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {handleEndsChange} placeholder="Ends" value={ends} required type="date" name ="ends" id="ends" className="form-control"></input>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange = {handleDescriptionChange} className="form-control" value={description} required name = "description" id ="description" rows="5"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {handleMaxPresChange} placeholder="Maximum presentations" value={maxPres} required type="number" name ="max_presentations" id="max_presentations" className="form-control"></input>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange = {handleMaxAttChange} placeholder="Maximum attendees" value={maxAtt} required type="number" name ="max_attendees" id="max_attendees" className="form-control"></input>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange = {handleLocationChange} required name = "location" value={location} id="location" className="form-select">
                  <option value="">Choose a location</option>
                    {locations.map(location => {
                        return (
                        <option key={location.id} value={location.id}>{location.name}</option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
