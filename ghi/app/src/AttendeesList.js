function AttendeesList(props) {
    return (
        <table className="table table-hover">
        <thead>
          <tr className="table-success">
            <th scope="col">Name</th>
            <th scope="col">Conference</th>
          </tr>
        </thead>
        <tbody>
          {props.attendees && props.attendees.map(attendee => {
            return (
              <tr className="table-primary" key={attendee.href}>
                <td>{attendee.name}</td>
                <td>{attendee.conference}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
}

export default AttendeesList;
