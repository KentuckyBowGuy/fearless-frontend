import React, { useEffect, useState } from 'react';

function PresentationForm () {
    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [company, setCompany] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.presenter_name = name;
        data.presenter_email = email;
        data.company_name = company;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = event.target.conference.value;

        console.log(data);

        const presentationUrl = `http://localhost:8000${conference}presentations/`;
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            setName('');
            setEmail('');
            setCompany('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    const handleCompChange = (event) => {
        const value = event.target.value;
        setCompany(value);
    }

    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const fetchData = async () => {
        const confUrl = 'http://localhost:8000/api/conferences/';

        const response = await fetch(confUrl);

        if (response.ok) {
            const confData = await response.json();
            setConferences(confData.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required type="text" name="presenter_name" id="presenter_name" className="form-control"></input>
                            <label htmlFor="presenter_name">Presenter Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmailChange} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"></input>
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCompChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"></input>
                            <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTitleChange} placeholder="Title" required type="text" name="title" id="title" className="form-control"></input>
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis" className="form-label">Synopsis</label>
                            <textarea onChange={handleSynopsisChange} className="form-control" required name="synopsis" id="synopsis" rows="3"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleConferenceChange} required name="conference" value={conference} id="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {conferences.map(conference => (
                                    <option key={conference.href} value={conference.href}>{conference.name}</option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-primary" type="submit">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default PresentationForm;
