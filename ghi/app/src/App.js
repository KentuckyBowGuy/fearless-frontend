import React from 'react';
import { Outlet, createBrowserRouter, RouterProvider } from 'react-router-dom';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  const router = createBrowserRouter([
    {
      path: '/',
      element: (
        <>
          <Nav />
          <div className="container">
            <Outlet />
          </div>
        </>
      ),
      children: [
        {
          index: true,
          element: <MainPage />,
        },
        {
          path: '/conferences/new',
          element: <ConferenceForm />,
        },
        {
          path: '/attendees/new',
          element: <AttendConferenceForm />,
        },
        {
          path: '/locations/new',
          element: <LocationForm />,
        },
        {
          path: '/attendees',
          element: <AttendeesList attendees={props.attendees} />,
        },
        {
          path: '/presentations/new',
          element: <PresentationForm />,
        },
      ]
    }
  ]);

  return <RouterProvider router={router} />;
}

export default App;
