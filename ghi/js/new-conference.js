


window.addEventListener('DOMContentLoaded', async () => {
    const locUrl = 'http://localhost:8000/api/locations/';
    const response = await fetch(locUrl);
    if (response.ok) {
        const locData = await response.json();

        const selectTag = document.getElementById('location');
        // For each state in the states property of the data
        for (let location of locData.locations) {
            // Create an 'option' element
            const option = document.createElement('option');
            // Set the '.value' property of the option element to the
            // state's abbreviation
            option.value = location.id;
            // Set the '.innerHTML' property of the option element to
            // the state's name
            option.innerHTML = location.name;
            // Append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
    }
    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener("submit", async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(Object.fromEntries(formData))
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        console.log(response);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);

        }

    });
});
