window.addEventListener('DOMContentLoaded', async () => {
    const confUrl = 'http://localhost:8000/api/conferences/';
    const response = await fetch(confUrl);
    if (response.ok) {
        const confData = await response.json();

        const selectTag = document.getElementById('conference');
        // For each conference in the conferences property of the data
        for (let conference of confData.conferences) {
            // Create an 'option' element
            const option = document.createElement('option');
            // Set the '.value' property of the option element to the
            // conference's id
            option.value = conference.id;
            // Set the '.innerHTML' property of the option element to
            // the conference's name
            option.innerHTML = conference.name;
            // Append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
    }
    const formTag = document.getElementById("create-presentation-form");
    formTag.addEventListener("submit", async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        console.log("Request Body:", Object.fromEntries(formData));
        const select = document.getElementById('conference');
        const conferenceId = select.value;
        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;

        const fetchConfig = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(Object.fromEntries(formData))
        };
        const response = await fetch(presentationUrl, fetchConfig);
        console.log(response);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation);
        }
    });
});
